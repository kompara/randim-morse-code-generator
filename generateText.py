#!/usr/bin/python
# -*- coding: utf-8 -*-

"""This is random Morse code generator."""

__author__  = "Tomaž Kompara"
__license__ = "GPL"
__version__ = "1.0.1"
__maintainer__ = "Tomaž Kompara"
__email__   = "tomaz@kompara.si"

from random import shuffle
import argparse
from morseAlphabet import morseAlphabet

def generateExamples(text_length, num_elements, symbols_dist, verbose):
  morse_tmp = morseAlphabet.keys()
  out_dict = {}

  while(1):
    shuffle(morse_tmp)
    count_signs = [0,0]
    morse_code = ""

    for j in range(text_length):
      morse_decode = morseAlphabet[morse_tmp[j]]
      morse_code += morse_decode + " | "
      count_signs[0]+=morse_decode.count(".")
      count_signs[1]+=morse_decode.count("-")

    if ((symbols_dist[0] == -1 or symbols_dist[0]== count_signs[0]) and
       (symbols_dist[1] == -1 or symbols_dist[1]== count_signs[1])):
      out_dict[str(count_signs)] = out_dict.get(str(count_signs), [0,[]])
      out_dict[str(count_signs)][0] += 1
      out_dict[str(count_signs)][1].append([morse_tmp[:text_length],morse_code[:-2]])
      if ((out_dict[str(count_signs)][0] >= num_elements)): break

  if verbose:
    for i,j in enumerate(out_dict[str(count_signs)][1]):
      print i+1, "\t\t",
      for c in j[0]: print c,
      print "\n\t\t",
      for c in j[1]: print c,
      print "\n"

  return count_signs, out_dict[str(count_signs)]

if __name__ == "__main__":
  symbols_dist = [-1,-1]
  parser = argparse.ArgumentParser(description='This is a random Morse code generator help.', add_help=False)
  requiredNamed = parser.add_argument_group('Required named arguments')
  requiredNamed.add_argument('-o','--output', metavar='filename', help='Name of output file (.csv)',required=True)
  optionalNamed = parser.add_argument_group('Optional named arguments')
  optionalNamed.add_argument('-n','--number', metavar='int', help='Number of examples to be generated (Default: 10)', default='10', required=False)
  optionalNamed.add_argument('-do','--dots', metavar='int', help='Number of dots in each example (Default: Random)', required=False)
  optionalNamed.add_argument('-da','--dashes', metavar='int', help='Number of dashes in each example (Default: Random)', required=False)
  optionalNamed.add_argument('-c','--characters', metavar='int', help='Number of characters in each example (Default: 5)', default='5',required=False)
  optionalNamed.add_argument("-v", "--verbose", action='store_true', help="Print output")
  optionalNamed.add_argument("-h", "--help", action="help", help="show this help message and exit")
  args = parser.parse_args()

  if args.number     != None: num_elements = int(args.number)
  if args.dots       != None: symbols_dist[0]=int(args.dots)
  if args.dashes     != None: symbols_dist[1]=int(args.dashes)
  if args.characters != None: text_length=int(args.characters)

  if (sum(symbols_dist) < (text_length * 3)) and (-1 not in symbols_dist):
    print """Warning: There may be no solution for your parameters. 
             \rTry to increase/decrease number of dots and dashes or change the number of characters."""

  count_signs, out_dict = generateExamples(text_length, num_elements, symbols_dist, args.verbose)

  # Print summary
  print "\nSummary:"
  print "\t* text length:\t\t", str(text_length)
  print "\t* number of '.':\t", str(count_signs[0])
  print "\t* number of '-':\t", str(count_signs[1])
  print "\t* number of examples:\t", str(out_dict[0])

  # Save it to file
  if args.output != None:
    f = open(args.output, "w")
    for i,j in enumerate(out_dict[1]):
      f.write(str(i+1) + ",")
      for c in j[0]: f.write(c)
      f.write(",")
      for c in j[1]: f.write(c)
      f.write("\n")
    f.close()