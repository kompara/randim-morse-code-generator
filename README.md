# Random Morse code generator

## Overview
This Python script generates predefined number of random examples for a predefined number of characters. This script was used to make random tasks for competition in sending Morse code. To ensure equal difficulty of all the tasks all examples have the same number of the symbols. Here are the output samples for 5 characters long words (9 dots and 11 dashes):


| Example number| Random word   | Morse code  | | |  | |                                                          |
| :------------:|:-------------:| :------------------------------:|:---:|:---:|:---:|:---:|
| 1             | AHX02         | . - \| . . . . \| - . . - \| - - - - - \| . . - - -  |
| 2             | 8G9DB         | - - - . . \| - - . \| - - - - . \| - . . \| - . . .  |
| 3             | WGZ78         | . - - \| - - . \| - - . . \| - - . . . \| - - - . .  |
| 4             | 3K7I0         | . . . - - \| - . - \| - - . . . \| . . \| - - - - -  |
| 5             | CKXZ2         | - . - . \| - . - \| - . . - \| - - . . \| . . - - -  |

**Warning**: The script may return a different number of each symbol in different runs, however <span style="text-decoration:underline">in each run all the examples will have the same number of symbols</span>. If you want a specific number of dots and/or dashes you have to pass them to the script (using -do and -da).

## Usage
`python generateText.py -o filename [-n int] [-do int] [-da int] [-c int] [-v][-h]`

Required named arguments:

*  `-o filename, --output filename`  Name of output file (.csv)

Optional named arguments:

*  `-n int, --number int`  Number of examples to be generated (Default: 10)

*  `-do int, --dots int`   Number of dots in each example (Default: Random)

*  `-da int, --dashes int` Number of dashes in each example (Default: Random)

*  `-c int, --characters int` Number of characters in each example (Default: 5)

*  `-v, --verbose`         Print output

*  `-h, --help`            Show help message and exit

## Examples
### Simplest example
It will generate default number of examples (10) with a default number of characters (5). All examples will have the same number of dots and dashes. All tasks will be saved in *tasks.csv*.

`python generateText.py -o tasks.csv`

### Medium example
You can define the number of examples (30) and the number of characters (10).

`python generateText.py -o tasks.csv -n 30 -c 10`

### Advanced example
In addition, you can define the number of dots (10) and/or dashes(20) (this may take a while).

`python generateText.py -o tasks.csv -n 30 -c 10 -do 10`

`python generateText.py -o tasks.csv -n 30 -c 10 -da 20`

`python generateText.py -o tasks.csv -n 30 -c 10 -do 10 -da 20`

## TODO
* ~~Comment the code.~~
* ~~Allow defining number of each symbol.~~
* ~~Automatically generate text file.~~
* ~~Allow defining the number of each symbol.~~

If you have any suggestion please contact me.

## Author
* Tomaž Kompara ([tomaz.kompara.si](http://www.tomaz.kompara.si))